﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSE445_A6_Services
{
    class BookResponseModel
    {
        public string kind { get; set; }
        public string Totalitems { get; set; }
        public List<Item> items { get; set; }
    }

    class Item
    {
        public VolumeInfo volumeInfo { get; set; }
    }

    class VolumeInfo
    {
        public string title { get; set; }
        public List<string> authors { get; set; }

        public string publishedDate { get; set; }

        public string description { get; set; }

        public string pageCount { get; set; }

        public string averageRating { get; set; }

        public string infoLink { get; set; }
    }
}