﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace CSE445_A6_Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string GetBook(string isbn)
        {
            WebRequest request = WebRequest.Create("https://www.googleapis.com/books/v1/volumes?q=isbn:"+isbn);
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Display the status.
            Console.WriteLine(response.StatusCode);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            string title = "";
            string author = "";
            string publishedDate = "";
            string description = "";
            string pageCount = "";
            string averageRating = "";
            string infoLink = "";
            var book = new JavaScriptSerializer().Deserialize<BookResponseModel>(responseFromServer);
            if (book.items != null && book.items[0].volumeInfo != null)
            {
                VolumeInfo vi = book.items[0].volumeInfo;
                if (vi.title != null)
                {
                    title = vi.title;
                }
                if (vi.authors != null)
                {
                    author = vi.authors[0];
                }
                if (vi.publishedDate != null)
                {
                    publishedDate = vi.publishedDate;
                }
                if (vi.description != null)
                {
                    description= vi.description;
                }
                if (vi.pageCount != null)
                {
                    pageCount = vi.pageCount;
                }
                if (vi.averageRating != null)
                {
                    averageRating = vi.averageRating;
                }
                if (vi.infoLink != null)
                {
                    infoLink = vi.infoLink;
                }
            }
            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            response.Close();
            return title + '/'
                   + author + '/'
                   + publishedDate + '/'
                   + description + '/'
                   + pageCount + '/'
                   + averageRating + '/'
                   + infoLink;
        }

        public string GetBookDescription(string title)
        {
            WebRequest request = WebRequest.Create("https://www.googleapis.com/books/v1/volumes?q=intitle:" + title);
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Display the status.
            Console.WriteLine(response.StatusCode);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            string desc = "";
            var bookResponse = new JavaScriptSerializer().Deserialize<BookResponseModel>(responseFromServer);
            if (bookResponse.items != null && bookResponse.items[0].volumeInfo != null && bookResponse.items[0].volumeInfo.description != null)
            {
                desc = bookResponse.items[0].volumeInfo.description;
            }
            reader.Close();
            dataStream.Close();
            response.Close();
            return desc;
        }

        public string[] Get10Books(string subject)
        {
            WebRequest request = WebRequest.Create("https://www.googleapis.com/books/v1/volumes?q=subject:" + subject);
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Display the status.
            Console.WriteLine(response.StatusCode);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            string title = "";
            string author = "";
            string publishedDate = "";
            string pageCount = "";
            string averageRating = "";
            string infoLink = "";
            string[] books = null;
            var bookResponse = new JavaScriptSerializer().Deserialize<BookResponseModel>(responseFromServer);
            if (bookResponse.items != null)
            {
                books = new string[bookResponse.items.Count];
                for (int i = 0; i < bookResponse.items.Count && i < 10; i++)
                {
                    if (bookResponse.items[i].volumeInfo != null)
                    {
                        VolumeInfo vi = bookResponse.items[i].volumeInfo;
                        if (vi.title != null)
                        {
                            title = vi.title;
                        }
                        if (vi.authors != null)
                        {
                            author = vi.authors[0];
                        }
                        if (vi.publishedDate != null)
                        {
                            publishedDate = vi.publishedDate;
                        }
                        if (vi.pageCount != null)
                        {
                            pageCount = vi.pageCount;
                        }
                        if (vi.averageRating != null)
                        {
                            averageRating = vi.averageRating;
                        }
                        if (vi.infoLink != null)
                        {
                            infoLink = vi.infoLink;
                        }
                        books[i] = title + '/'
                            + author + '/'
                            + publishedDate + '/'
                            + pageCount + '/'
                            + averageRating + '/'
                            + infoLink;
                    }
                }
            }
            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            response.Close();
            return books;
        }
        //The input format to this function will be book data strings like:
        //title0/publishedDate0/pageCount0/averageRating0/infoLink0$title1/publishedDate1/pageCount1/averageRating1/infoLink1
        //where each book is separated by a $
        public string[] SortBooks(string b, int searchTerm)//search term can be 0 - 6
        {
            string[] books = b.Split('$');
            Sort(books, 0, books.Length - 1, searchTerm);
            return books;
        }

        private void Sort(string[] strings, int l, int r, int st)
        {
            int m;
            if (r > l)
            {
                m = (r + l) / 2;
                Sort(strings, l, m, st);
                Sort(strings, (m + 1), r, st);
                Merge(strings, l, (m + 1), r, st);
            }
        }

        private void Merge(string[] s, int l, int m, int r, int st)
        {
            string[] temp = new string[s.Length];
            int i, eol, num, pos;
            eol = (m - 1);
            pos = l;
            num = (r - l + 1);
            string[] c1 = null;
            string[] c2 = null;
            while ((l <= eol) && (m <= r))
            {
                c1 = s[l].Split('/');
                c2 = s[m].Split('/');
                if (c1[st].CompareTo(c2[st]) <= 0)
                    temp[pos++] = s[l++];
                else
                    temp[pos++] = s[m++];
            }
            while (l <= eol)
                temp[pos++] = s[l++];
            while (m <= r)
                temp[pos++] = s[m++];
            for (i = 0; i < num; i++)
            {
                s[r] = temp[r];
                r--;
            }
        }
    }
}
