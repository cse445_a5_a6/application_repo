﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CSE445_A6_Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        // TODO: Add your service operations here
        [WebGet]
        [OperationContract]
        string GetBook(string isbn);

        [WebGet]
        [OperationContract]
        string[] Get10Books(string searchTerm);

        [WebGet]
        [OperationContract]
        string GetBookDescription(string title);

        [WebGet(UriTemplate = "SortBooks?sortBy={sortBy}&searchTerm={searchTerm}")]
        [OperationContract]
        string[] SortBooks(string sortBy, int searchTerm);
    }
}
